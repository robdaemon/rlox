use std::env;
use std::fs;
use std::process::exit;

use crate::scanner::new_scanner;
use crate::scanner::Scanner;

pub mod scanner;

fn run(code: &String) {
    let mut s: Scanner = new_scanner(code.to_string());
    let tokens = s.scan_tokens();

    for t in tokens {
        println!("{:?} {:?} {:?}", t.token_type, t.lexeme, t.literal);
    }
}

fn run_file(file_path: &String) {
    let contents = fs::read_to_string(file_path).expect("Should have been able to read the file");

    run(&contents);
}

fn run_prompt() {
    println!("prompt here");
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() > 2 {
        println!("Usage: rlox [script]");
        exit(64);
    } else if args.len() == 2 {
        run_file(&args[1]);
    } else {
        run_prompt();
    }
}
