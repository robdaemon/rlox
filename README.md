[![Build status](https://ci.codeberg.org/api/badges/robdaemon/rlox/status.svg?branch=main)](https://ci.codeberg.org/robdaemon/rlox)

# Crafting Interpreters in Rust

This is the Lox scripting language from Robert Nystrom's book "Crafting Interpreters" written in Rust (instead of Java)

